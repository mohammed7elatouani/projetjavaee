package app.config;



import core.config.CoreConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({CoreConfig.class, AppConfig.class, WebConfig.class})
public class AppConfig {
}
